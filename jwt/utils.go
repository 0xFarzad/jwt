package jwt

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"net/http"
	"time"
)

var (
	yellow = customLog("\n\033[1;33m%s\033[0m")
	info   = yellow
)

// customLog makes output colorful & add time.
func customLog(colorString string) func(...interface{}) string {
	sprint := func(args ...interface{}) string {
		currentTime := time.Now().Format("2006-01-02 15:04:05")
		return fmt.Sprintf(colorString, fmt.Sprint(currentTime, " | ")) + fmt.Sprintln(args...)
	}
	return sprint
}

// generateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// generateRandomStringURLSafe returns a URL-safe, base64 encoded
// securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func generateRandomStringURLSafe(n int) (string, error) {
	b, err := generateRandomBytes(n)
	return base64.URLEncoding.EncodeToString(b), err
}

func generateNewCsrfString() (string, *jwtError) {
	newCsrf, err := generateRandomStringURLSafe(32)
	if err != nil {
		return "", newJwtError(err, 500)
	}

	return newCsrf, nil
}

func setHeader(w http.ResponseWriter, header string, value string) {
	w.Header().Set(header, value)
}

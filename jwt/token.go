package jwt

import (
	"errors"
	"log"
	"net/http"
	"time"

	jwtGo "github.com/dgrijalva/jwt-go"
)

type jwtToken struct {
	Token    *jwtGo.Token
	ParseErr error
	options  tokenOptions
}

type tokenOptions struct {
	ValidTime           time.Duration
	SigningMethodString string
	Debug               bool
}

func (t *jwtToken) myLog(stools interface{}) {
	if t.options.Debug {
		log.Println(info(stools))
	}
}

func (t *jwtToken) updateTokenExpiry() *jwtError {
	tokenClaims, ok := t.Token.Claims.(*ClaimsType)
	if !ok {
		return newJwtError(errors.New("Cannot read token claims"), http.StatusInternalServerError)
	}

	tokenClaims.StandardClaims.ExpiresAt = time.Now().Add(t.options.ValidTime).Unix()

	// update the token
	t.Token = jwtGo.NewWithClaims(jwtGo.GetSigningMethod(t.options.SigningMethodString), tokenClaims)

	return nil
}

func (t *jwtToken) updateTokenCsrf(csrfString string) *jwtError {
	tokenClaims, ok := t.Token.Claims.(*ClaimsType)
	if !ok {
		return newJwtError(errors.New("Cannot read token claims"), http.StatusInternalServerError)
	}

	tokenClaims.Csrf = csrfString

	// update the token
	t.Token = jwtGo.NewWithClaims(jwtGo.GetSigningMethod(t.options.SigningMethodString), tokenClaims)

	return nil
}

func (t *jwtToken) updateTokenExpiryAndCsrf(csrfString string) *jwtError {
	tokenClaims, ok := t.Token.Claims.(*ClaimsType)
	if !ok {
		return newJwtError(errors.New("Cannot read token claims"), http.StatusInternalServerError)
	}

	tokenClaims.StandardClaims.ExpiresAt = time.Now().Add(t.options.ValidTime).Unix()
	tokenClaims.Csrf = csrfString

	// update the token
	t.Token = jwtGo.NewWithClaims(jwtGo.GetSigningMethod(t.options.SigningMethodString), tokenClaims)

	return nil
}

package jwt

import (
	"context"
	"errors"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	jwtGo "github.com/dgrijalva/jwt-go"
)

type contextKey string

const contextErrCodeKey contextKey = "errorCode"

// TokenRevoker : a type to revoke tokens
type TokenRevoker func(tokenId string) error

// TokenIDChecker : a type to check tokens
type TokenIDChecker func(tokenId string) bool

// ClaimsType : holds the claims encoded in the jwt
type ClaimsType struct {
	// Standard claims are the standard jwt claims from the ietf standard
	// https://tools.ietf.org/html/rfc7519
	jwtGo.StandardClaims
	Csrf         string
	CustomClaims map[string]interface{}
}

// Auth is a middleware that provides jwt based authentication.
type Auth struct {
	signKey   interface{}
	verifyKey interface{}
	options   Options

	// Handlers for when an error occurs
	errorHandler http.Handler

	// funcs for checking and revoking refresh tokens
	revokeRefreshToken TokenRevoker
	checkTokenID       TokenIDChecker
}

// New constructs a new Auth instance with supplied options.
func New(opts ...Options) (*Auth, error) {
	// Init config
	var o Options
	if len(opts) > 0 {
		o = opts[0]
	}

	// check if durations have been provided for auth and refresh token exp
	// if not, set them equal to the default
	if o.RefreshTokenValidTime <= 0 {
		o.RefreshTokenValidTime = defaultRefreshTokenValidTime
	}
	if o.AuthTokenValidTime <= 0 {
		o.AuthTokenValidTime = defaultAuthTokenValidTime
	}

	if o.BearerTokens {
		if o.AuthTokenName == "" {
			o.AuthTokenName = defaultBearerAuthTokenName
		}

		if o.RefreshTokenName == "" {
			o.RefreshTokenName = defaultBearerRefreshTokenName
		}
	} else {
		if o.AuthTokenName == "" {
			o.AuthTokenName = defaultCookieAuthTokenName
		}

		if o.RefreshTokenName == "" {
			o.RefreshTokenName = defaultCookieRefreshTokenName
		}
	}

	if o.CSRFTokenName == "" {
		o.CSRFTokenName = defaultCSRFTokenName
	}

	// create the sign and verify keys
	signKey, verifyKey, err := o.buildSignAndVerifyKeys()
	if err != nil {
		return nil, err
	}

	auth := &Auth{
		signKey:            signKey,
		verifyKey:          verifyKey,
		options:            o,
		errorHandler:       http.HandlerFunc(defaultErrorHandler),
		revokeRefreshToken: TokenRevoker(defaultTokenRevoker),
		checkTokenID:       TokenIDChecker(defaultCheckTokenID),
	}

	return auth, nil
}

// SetErrorHandler : add methods to allow the changing of default functions
func (a *Auth) SetErrorHandler(handler http.Handler) {
	a.errorHandler = handler
}

// SetRevokeTokenFunction : set the function which revokes a token
func (a *Auth) SetRevokeTokenFunction(revoker TokenRevoker) {
	a.revokeRefreshToken = revoker
}

// SetCheckTokenIDFunction : set the function which checks token id's
func (a *Auth) SetCheckTokenIDFunction(checker TokenIDChecker) {
	a.checkTokenID = checker
}

// Handler implements the http.HandlerFunc for integration with the standard net/http lib.
func (a *Auth) Handler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Process the request. If it returns an error,
		// that indicates the request should not continue.
		if jwtErr := a.process(w, r); jwtErr != nil {
			a.myLog("Error processing jwts\n" + jwtErr.Error())
			_ = a.NullifyTokens(w, r)

			ctx := context.WithValue(r.Context(), contextErrCodeKey, jwtErr.Type)
			a.errorHandler.ServeHTTP(w, r.WithContext(ctx))
			return
		}

		h.ServeHTTP(w, r)
	})
}

// HandlerFunc works identically to Handler, but takes a HandlerFunc instead of a Handler.
func (a *Auth) HandlerFunc(fn http.HandlerFunc) http.Handler {
	if fn == nil {
		return a.Handler(nil)
	}
	return a.Handler(fn)
}

// HandlerFuncWithNext is a special implementation for Negroni, but could be used elsewhere.
func (a *Auth) HandlerFuncWithNext(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	jwtErr := a.process(w, r)
	if jwtErr == nil && next != nil {
		next(w, r)
		return
	}

	a.myLog("Error processing jwt\n" + jwtErr.Error())

	_ = a.NullifyTokens(w, r)

	ctx := context.WithValue(r.Context(), contextErrCodeKey, jwtErr.Type)
	a.errorHandler.ServeHTTP(w, r.WithContext(ctx))
}

// Process runs the actual checks and returns an error if the middleware chain should stop.
func (a *Auth) process(w http.ResponseWriter, r *http.Request) *jwtError {
	// cookies aren't included with options, so simply pass through
	if r.Method == "OPTIONS" {
		a.myLog("Method is OPTIONS")
		return newJwtError(errors.New("Method Is Not Allowed"), http.StatusMethodNotAllowed)
	}

	// grab the credentials from the request
	var c credentials
	if err := a.buildCredentialsFromRequest(r, &c); err != nil {
		return newJwtError(err, http.StatusInternalServerError)
	}

	// check the credential's validity; updating expiry's if necessary and/or allowed
	if err := c.validateAndUpdateCredentials(); err != nil {
		return newJwtError(err, http.StatusInternalServerError)
	}

	a.myLog("Successfully checked / refreshed all jwt")

	// if we've made it this far, everything is valid!
	// And tokens have been refreshed if need-be
	if !a.options.VerifyOnlyServer {
		if err := a.setCredentialsOnResponseWriter(w, &c); err != nil {
			return newJwtError(err, http.StatusInternalServerError)
		}
	}

	return nil
}

// IssueNewTokens : and also modify create refresh and auth token functions!
func (a *Auth) IssueNewTokens(w http.ResponseWriter, claims *ClaimsType) error {
	if a.options.VerifyOnlyServer {
		a.myLog("Server is not authorized to issue new tokens")
		return errors.New("Server is not authorized to issue new tokens")
	}

	c, err := a.buildCredentialsFromClaims(claims)
	if err != nil {
		return errors.New(err.Error())
	}

	err = a.setCredentialsOnResponseWriter(w, c)
	if err != nil {
		return errors.New(err.Error())
	}

	return nil
}

// NullifyTokens : invalidate tokens
// note @adam-hanna: what if there are no credentials in the request?
func (a *Auth) NullifyTokens(w http.ResponseWriter, r *http.Request) error {
	var c credentials
	err := a.buildCredentialsFromRequest(r, &c)
	if err != nil {
		a.myLog("Err building credentials\n" + err.Error())
		return errors.New(err.Error())
	}

	if a.options.BearerTokens {
		// tokens are not in cookies
		setHeader(w, a.options.AuthTokenName, "")
		setHeader(w, a.options.RefreshTokenName, "")
	} else {
		authCookie := http.Cookie{
			Name:     a.options.AuthTokenName,
			Value:    "",
			Expires:  time.Now().Add(-1000 * time.Hour),
			HttpOnly: true,
			Secure:   !a.options.IsDevEnv,
		}

		http.SetCookie(w, &authCookie)

		refreshCookie := http.Cookie{
			Name:     a.options.RefreshTokenName,
			Value:    "",
			Expires:  time.Now().Add(-1000 * time.Hour),
			HttpOnly: true,
			Secure:   !a.options.IsDevEnv,
		}

		http.SetCookie(w, &refreshCookie)
	}

	refreshTokenClaims := c.RefreshToken.Token.Claims.(*ClaimsType)
	a.revokeRefreshToken(refreshTokenClaims.StandardClaims.Id)

	setHeader(w, a.options.CSRFTokenName, "")
	setHeader(w, "Auth-Expiry", strconv.FormatInt(time.Now().Add(-1000*time.Hour).Unix(), 10))
	setHeader(w, "Refresh-Expiry", strconv.FormatInt(time.Now().Add(-1000*time.Hour).Unix(), 10))

	a.myLog("Successfully nullified tokens and csrf string")
	return nil
}

// GrabTokenClaims : extract the claims from the request
// note: we always grab from the authToken
func (a *Auth) GrabTokenClaims(r *http.Request) (ClaimsType, error) {
	var c credentials
	err := a.buildCredentialsFromRequest(r, &c)
	if err != nil {
		a.myLog("Err grabbing credentials \n" + err.Error())
		return ClaimsType{}, errors.New(err.Error())
	}

	return *c.AuthToken.Token.Claims.(*ClaimsType), nil
}

func (a *Auth) buildCredentialsFromClaims(claims *ClaimsType) (*credentials, *jwtError) {
	newCsrfString, err := generateNewCsrfString()
	if err != nil {
		return nil, newJwtError(err, http.StatusInternalServerError)
	}
	var c credentials
	c.CsrfString = newCsrfString

	c.options.AuthTokenValidTime = a.options.AuthTokenValidTime
	c.options.RefreshTokenValidTime = a.options.RefreshTokenValidTime
	c.options.CheckTokenID = a.checkTokenID
	c.options.VerifyOnlyServer = a.options.VerifyOnlyServer
	c.options.SigningMethodString = a.options.SigningMethodString
	c.options.Debug = a.options.Debug

	authClaims := *claims
	authClaims.Csrf = newCsrfString
	authClaims.StandardClaims.ExpiresAt = time.Now().Add(a.options.AuthTokenValidTime).Unix()
	c.AuthToken = c.newTokenWithClaims(&authClaims, a.options.AuthTokenValidTime)

	refreshClaimsClaims := *claims
	refreshClaimsClaims.Csrf = newCsrfString
	refreshClaimsClaims.StandardClaims.ExpiresAt = time.Now().Add(a.options.RefreshTokenValidTime).Unix()
	c.RefreshToken = c.newTokenWithClaims(&refreshClaimsClaims, a.options.RefreshTokenValidTime)

	return &c, nil
}

func (a *Auth) buildCredentialsFromStrings(csrfString string, authTokenString string, refreshTokenString string, c *credentials) *jwtError {
	c.CsrfString = csrfString

	c.options.AuthTokenValidTime = a.options.AuthTokenValidTime
	c.options.RefreshTokenValidTime = a.options.RefreshTokenValidTime
	c.options.CheckTokenID = a.checkTokenID
	c.options.VerifyOnlyServer = a.options.VerifyOnlyServer
	c.options.SigningMethodString = a.options.SigningMethodString
	c.options.Debug = a.options.Debug

	// Note: Don't check for errors because it will be done later
	//       Also, tokens that have expired will throw err?
	c.AuthToken = c.buildTokenWithClaimsFromString(authTokenString, a.verifyKey, a.options.AuthTokenValidTime)

	if refreshTokenString != "" {
		c.RefreshToken = c.buildTokenWithClaimsFromString(refreshTokenString, a.verifyKey, a.options.RefreshTokenValidTime)
	}

	return nil
}

func (a *Auth) buildCredentialsFromRequest(r *http.Request, c *credentials) *jwtError {
	authTokenString, refreshTokenString, err := a.extractTokenStringsFromReq(r)
	if err != nil {
		return newJwtError(err, http.StatusInternalServerError)
	}

	csrfString, err := a.extractCsrfStringFromReq(r)
	if err != nil {
		return newJwtError(err, http.StatusInternalServerError)
	}

	err = a.buildCredentialsFromStrings(csrfString, authTokenString, refreshTokenString, c)
	if err != nil {
		return newJwtError(err, http.StatusInternalServerError)
	}

	return nil
}

// return is (authTokenString, refreshTokenString, err)
func (a *Auth) extractTokenStringsFromReq(r *http.Request) (string, string, *jwtError) {
	// read cookies
	if a.options.BearerTokens {
		// tokens are not in cookies
		// Note: we don't check for errors here, because we will check if the token is valid, later
		return r.Header.Get(a.options.AuthTokenName), r.Header.Get(a.options.RefreshTokenName), nil
	}

	var (
		authCookieValue    string
		refreshCookieValue string
	)

	AuthCookie, authErr := r.Cookie(a.options.AuthTokenName)
	if authErr == http.ErrNoCookie {
		a.myLog("Unauthorized attempt! No auth cookie")
		return "", "", newJwtError(errors.New("No auth cookie"), http.StatusUnauthorized)
	}
	if authErr != nil {
		// a.myLog(authErr)
		return "", "", newJwtError(errors.New("Internal Server Error"), http.StatusInternalServerError)
	}

	RefreshCookie, refreshErr := r.Cookie(a.options.RefreshTokenName)
	if refreshErr != nil && refreshErr != http.ErrNoCookie {
		a.myLog(refreshErr)
		return "", "", newJwtError(errors.New("Internal Server Error"), http.StatusInternalServerError)
	}

	if AuthCookie != nil {
		authCookieValue = AuthCookie.Value
	}
	if RefreshCookie != nil {
		refreshCookieValue = RefreshCookie.Value
	}

	return authCookieValue, refreshCookieValue, nil
}

func (a *Auth) extractCsrfStringFromReq(r *http.Request) (string, *jwtError) {
	csrfString := r.FormValue(a.options.CSRFTokenName)

	if csrfString != "" {
		return csrfString, nil
	}

	if csrfString = r.Header.Get(a.options.CSRFTokenName); csrfString != "" {
		return csrfString, nil
	}

	auth := r.Header.Get("Authorization")
	csrfString = strings.Replace(auth, "Bearer", "", 1)
	csrfString = strings.Replace(csrfString, " ", "", -1)
	if csrfString == "" {
		return csrfString, newJwtError(errors.New("No CSRF string"), 401)
	}

	return csrfString, nil
}

func (a *Auth) setCredentialsOnResponseWriter(w http.ResponseWriter, c *credentials) *jwtError {
	var (
		refreshTokenString string
		refreshTokenClaims *ClaimsType
	)

	authTokenString, err := c.AuthToken.Token.SignedString(a.signKey)
	if err != nil {
		return newJwtError(err, http.StatusInternalServerError)
	}
	if c.RefreshToken != nil && c.RefreshToken.Token != nil {
		a.myLog(c.RefreshToken)
		a.myLog(c.RefreshToken.Token)
		refreshTokenString, err = c.RefreshToken.Token.SignedString(a.signKey)
		if err != nil {
			return newJwtError(err, http.StatusInternalServerError)
		}
	}

	if a.options.BearerTokens {
		// tokens are not in cookies
		setHeader(w, a.options.AuthTokenName, authTokenString)
		if refreshTokenString != "" {
			setHeader(w, a.options.RefreshTokenName, refreshTokenString)
		}
	} else {
		// tokens are in cookies
		// note: don't use an "Expires" in auth cookies bc browsers won't send expired cookies?
		authCookie := http.Cookie{
			Name:  a.options.AuthTokenName,
			Value: authTokenString,
			Path:  "/",
			// Expires:  time.Now().Add(a.options.AuthTokenValidTime),
			HttpOnly: true,
			Secure:   !a.options.IsDevEnv,
		}
		http.SetCookie(w, &authCookie)

		if refreshTokenString != "" {
			refreshCookie := http.Cookie{
				Name:     a.options.RefreshTokenName,
				Value:    refreshTokenString,
				Expires:  time.Now().Add(a.options.RefreshTokenValidTime),
				Path:     "/",
				HttpOnly: true,
				Secure:   !a.options.IsDevEnv,
			}
			http.SetCookie(w, &refreshCookie)
		}
	}

	authTokenClaims, ok := c.AuthToken.Token.Claims.(*ClaimsType)
	if !ok {
		a.myLog("Cannot read auth token claims")
		return newJwtError(errors.New("Cannot read token claims"), http.StatusInternalServerError)
	}
	if c.RefreshToken != nil && c.RefreshToken.Token != nil {
		refreshTokenClaims, ok = c.RefreshToken.Token.Claims.(*ClaimsType)
		if !ok {
			a.myLog("Cannot read refresh token claims")
			return newJwtError(errors.New("Cannot read token claims"), http.StatusInternalServerError)
		}
	}

	w.Header().Set(a.options.CSRFTokenName, c.CsrfString)
	// note @adam-hanna: this may not be correct when using a sep auth server?
	//    							 bc it checks the request?
	w.Header().Set("Auth-Expiry", strconv.FormatInt(authTokenClaims.StandardClaims.ExpiresAt, 10))
	if refreshTokenClaims != nil {
		w.Header().Set("Refresh-Expiry", strconv.FormatInt(refreshTokenClaims.StandardClaims.ExpiresAt, 10))
	}

	return nil
}

func (a *Auth) myLog(stools interface{}) {
	if a.options.Debug {
		log.Println(info(stools))
	}
}
